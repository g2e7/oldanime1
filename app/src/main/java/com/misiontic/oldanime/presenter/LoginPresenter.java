package com.misiontic.oldanime.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import com.misiontic.oldanime.model.LoginInteractor;
import com.misiontic.oldanime.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        boolean error = false;
        // Validar datos
        view.showEmailError("");
        view.showPasswordError("");
        if (loginInfo.getPassword().isEmpty()) {
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }
        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es obligatorio");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios");
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> {
                model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                        new LoginMVP.Model.validateCredentialsCallbacks() {
                            @Override
                            public void onSuccess() {
                                SharedPreferences preferences = view.getActivity()
                                        .getSharedPreferences("auth", Context.MODE_PRIVATE );
                                preferences.edit()
                                        .putBoolean("logged", true)
                                        .apply();

                                view.getActivity().runOnUiThread(() -> {
                                    view.hideProgressBar();
                                    view.showInicioActivity();
                                });
                            }

                            @Override
                            public void onFailure() {
                                view.getActivity().runOnUiThread(() -> {
                                    view.hideProgressBar();
                                    view.showGenerarError("Credenciales Inválidas");
                                });
                            }
                        });
            }).start();

        }
    }

    @Override
    public void onPasswordClick() {
        view.showOlvidoActivity();

    }


    @Override
    public void onCrearClick() {
        view.showCrearActivity();

    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com");
    }

    @Override
    public void onGoogleClick() {

    }

    @Override
    public void isLoggedUser() {
        SharedPreferences preferences = view.getActivity()
                .getSharedPreferences("auth", Context.MODE_PRIVATE);

        Boolean islogged = preferences.getBoolean("logged", false);

        if (islogged) {
            view.showInicioActivity();
        }
    }

}
