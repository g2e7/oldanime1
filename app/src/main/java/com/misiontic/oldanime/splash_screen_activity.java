package com.misiontic.oldanime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import com.misiontic.oldanime.view.login.acceder_activity;

public class splash_screen_activity extends AppCompatActivity {

    private final int Duracion_Splash = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_splash_screen);

        ImageView imagen = findViewById(R.id.iv_nombre);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(splash_screen_activity.this, acceder_activity.class);
                startActivity(intent);
                finish();
             }
        }, Duracion_Splash);




    }


}