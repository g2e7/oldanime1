package com.misiontic.oldanime;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.misiontic.oldanime.databinding.ActivityPlayerBinding;

public class PlayerActivity extends AppCompatActivity{

    VideoView video_View;
    MediaController mediaController;
    int position  = 0;


    @Override

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_player);


       View decorView = getWindow().getDecorView();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

        video_View = findViewById(R.id.video_View);

        //  String path = "android.resource://"+getPackageName()+"/"+R.raw.triguncap1;

        video_View.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.jujutsucap1));
        video_View.start();

        if(this.mediaController == null){
            this.mediaController = new MediaController(PlayerActivity.this);

            this.mediaController.setAnchorView(video_View);

            this.video_View.setMediaController(mediaController);

            this.video_View.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    video_View.seekTo(position);
                    if (position == 0){
                        video_View.start();
                    }
                }
            });
        }


    }

}