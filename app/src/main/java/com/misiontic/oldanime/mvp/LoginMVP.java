package com.misiontic.oldanime.mvp;

import android.app.Activity;

import com.misiontic.oldanime.databinding.ActivityAccederBinding;

public interface LoginMVP {
    interface Model {

        void validateCredentials(String email, String password, validateCredentialsCallbacks callback);

        interface validateCredentialsCallbacks{
            void onSuccess();
            void onFailure();
        }
    }

    interface Presenter {
        void onLoginClick();
        void onGoogleClick();

        void isLoggedUser();

        void onCrearClick();

        void onPasswordClick();
    }

    interface View {

        Activity getActivity();

        LoginInfo getLoginInfo();
        void showEmailError(String error);
        void showPasswordError(String error);

        void showInicioActivity();

        void showGenerarError(String error);

        void showProgressBar();

        void hideProgressBar();

        void showOlvidoActivity();

        void showCrearActivity();
    }

    class LoginInfo {
        private String email;
        private String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
