package com.misiontic.oldanime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Switch;

public class Quintillizas extends AppCompatActivity {

    Switch cap1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quintillizas);

        cap1 = findViewById(R.id.cap_1);
        cap1.setOnClickListener(v -> cap());
    }


    public void cap(){
        if(cap1.getId()==R.id.cap_1){
            if (cap1.isChecked()){
                Intent intent = new Intent( Quintillizas.this, reproductor_one_piece.class);
                startActivity(intent);

            }
        }
    }
}