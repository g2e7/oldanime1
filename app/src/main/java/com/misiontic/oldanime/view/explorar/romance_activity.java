package com.misiontic.oldanime.view.explorar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

import com.misiontic.oldanime.Quintillizas;
import com.misiontic.oldanime.R;

public class romance_activity extends AppCompatActivity {

    ImageButton b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_romance);

        b1 = findViewById(R.id.ib_quintillizas);
        b1.setOnClickListener(v -> quintillizas());
    }

    private void quintillizas() {
        startActivity(new Intent(romance_activity.this, Quintillizas.class));
    }
}