package com.misiontic.oldanime.view.inicio;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misiontic.oldanime.PlayerActivity;
import com.misiontic.oldanime.R;

import com.misiontic.oldanime.view.explorar.category_activity;
import com.misiontic.oldanime.view.listas.lista_activity;
import com.misiontic.oldanime.view.ranking.ranking_activity;

public class inicio_activity extends AppCompatActivity {

    ImageButton ib1;
    TextView usuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);





        ib1 = findViewById(R.id.ib_continuarviendo);
        ib1.setOnClickListener(v -> continuar_viendo());


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setSelectedItemId((R.id.page_1));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        startActivity(new Intent(getApplicationContext(), inicio_activity.class));
                        finish();
                        overridePendingTransition(0, 0);
                        return false;


                    case R.id.page_2:
                        startActivity(new Intent(getApplicationContext(), lista_activity.class));
                        finish();
                        overridePendingTransition(0, 0);
                        return false;

                    case R.id.page_3:
                        startActivity(new Intent(getApplicationContext(), category_activity.class));
                        finish();
                        overridePendingTransition(0, 0);
                        return false;

                    case R.id.page_4:
                        startActivity(new Intent(getApplicationContext(), ranking_activity.class));
                        finish();
                        overridePendingTransition(0, 0);
                        return false;
                }
                return false;
            }
        });

    }

    private void continuar_viendo() {
        startActivity(new Intent(inicio_activity.this, PlayerActivity.class));
    }


}