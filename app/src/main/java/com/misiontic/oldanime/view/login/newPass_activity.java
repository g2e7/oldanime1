package com.misiontic.oldanime.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.misiontic.oldanime.R;

public class newPass_activity extends AppCompatActivity {

    AppCompatButton btnRecuperar;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pass);

        btnRecuperar = findViewById(R.id.btn_guardarPass);
        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(newPass_activity.this, acceder_activity.class);
                startActivity(intent);
            }
        });
    }
}