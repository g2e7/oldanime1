package com.misiontic.oldanime.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.misiontic.oldanime.R;

public class
crearCuenta_activity extends AppCompatActivity {

    AppCompatButton btnRegistrado;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);

        btnRegistrado = findViewById(R.id.btn_registrado);
        btnRegistrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(crearCuenta_activity.this, acceder_activity.class);
                startActivity(intent);
            }
        });
    }
}