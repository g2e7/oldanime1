package com.misiontic.oldanime.view.explorar;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.widget.ImageButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misiontic.oldanime.R;
import com.misiontic.oldanime.view.inicio.inicio_activity;
import com.misiontic.oldanime.view.listas.lista_activity;
import com.misiontic.oldanime.view.ranking.ranking_activity;


public class category_activity extends AppCompatActivity {

    ImageButton ib1,ib2,ib3,ib4,ib5,ib6,ib7,ib8,ib9,ib10,ib11 ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        ib1 =  findViewById(R.id.ib_aventura);
        ib1.setOnClickListener(v -> aventura());

        ib2 =  findViewById(R.id.ib_romance);
        ib2.setOnClickListener(v -> romance());

        ib3 =  findViewById(R.id.ib_accion);
        ib3.setOnClickListener(v -> accion());

        ib4 =  findViewById(R.id.ib_comedia);
        ib4.setOnClickListener(v -> comedia());

        ib5 =  findViewById(R.id.ib_terror);
        ib5.setOnClickListener(v -> terror());

        ib6 =  findViewById(R.id.ib_misterio);
        ib6.setOnClickListener(v -> misterio());

        ib7 =  findViewById(R.id.ib_drama);
        ib7.setOnClickListener(v -> drama());

        ib8 =  findViewById(R.id.ib_deportes);
        ib8.setOnClickListener(v -> deportes());

        ib9 =  findViewById(R.id.ib_musica);
        ib9.setOnClickListener(v -> musica());

        ib10 = findViewById(R.id.ib_sobrenatural);
        ib10.setOnClickListener(v -> sobrenatural());

        ib11 = findViewById(R.id.ib_ciencia);
        ib11.setOnClickListener(v -> ciencia());




        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setSelectedItemId((R.id.page_3));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.page_1:
                        startActivity(new Intent(getApplicationContext(), inicio_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;

                    case R.id.page_2:
                        startActivity(new Intent(getApplicationContext(), lista_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;


                    case R.id.page_3:
                        startActivity(new Intent(getApplicationContext(),category_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;



                    case R.id.page_4:
                        startActivity(new Intent(getApplicationContext(), ranking_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;



                }
                return false;
            }
        });


        }





    private void aventura() {
        Intent intent = new Intent(category_activity.this, aventura_activity.class);
        startActivity(intent);
    }

    private void romance() {
        Intent intent = new Intent(category_activity.this, romance_activity.class);
        startActivity(intent);
    }

    private void accion() {
        Intent intent = new Intent(category_activity.this, accion_activity.class);
        startActivity(intent);
    }

    private void comedia() {
        Intent intent = new Intent(category_activity.this, comedia_activity.class);
        startActivity(intent);
    }

    private void terror() {
        Intent intent = new Intent(category_activity.this, terror_activity.class);
        startActivity(intent);
    }

    private void misterio() {
        Intent intent = new Intent(category_activity.this, misterio_activity.class);
        startActivity(intent);
    }

    private void drama() {
        Intent intent = new Intent(category_activity.this, drama_activity.class);
        startActivity(intent);
    }

    private void deportes() {
        Intent intent = new Intent(category_activity.this, deportes_activity.class);
        startActivity(intent);
    }

    private void musica() {
        Intent intent = new Intent(category_activity.this, musica_activity.class);
        startActivity(intent);
    }

    private void sobrenatural() {
        Intent intent = new Intent(category_activity.this, sobrenatural_activity.class);
        startActivity(intent);
    }

    private void ciencia() {
        Intent intent = new Intent(category_activity.this, ciencia_activity.class);
        startActivity(intent);
    }
}

