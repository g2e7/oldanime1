package com.misiontic.oldanime.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misiontic.oldanime.R;
import com.misiontic.oldanime.mvp.LoginMVP;
import com.misiontic.oldanime.presenter.LoginPresenter;
import com.misiontic.oldanime.view.inicio.inicio_activity;

public class acceder_activity extends AppCompatActivity implements LoginMVP.View{

    private LinearProgressIndicator pbWait;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private AppCompatButton btnAcceder2, btnCrearCuenta2, btnOlvidarPass;

    LoginMVP.Presenter presenter;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceder);

        presenter = new LoginPresenter(this);
        presenter.isLoggedUser();

        initUI();
    }

    @SuppressLint("WrongViewCast")
    private void initUI() {
        pbWait = findViewById(R.id.pb_wait);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnAcceder2 = findViewById(R.id.btn_acceder2);
        btnAcceder2.setOnClickListener(v -> presenter.onLoginClick());

        btnCrearCuenta2 = findViewById(R.id.btn_crearCuenta2);
        btnCrearCuenta2.setOnClickListener(v -> presenter.onCrearClick());

        btnOlvidarPass = findViewById(R.id.btn_olvidarPass);
        btnOlvidarPass.setOnClickListener(v -> presenter.onPasswordClick());
    }

    private void onOlvidarClick() {
        Intent intent = new Intent(acceder_activity.this, olvidarPass_activity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return acceder_activity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showInicioActivity() {
        Intent intent = new Intent(acceder_activity.this, inicio_activity.class);
        startActivity(intent);
    }

    @Override
    public void showGenerarError(String error) {
        Toast.makeText(acceder_activity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnAcceder2.setEnabled(false);
        btnCrearCuenta2.setEnabled(false);
        btnOlvidarPass.setEnabled(false);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnAcceder2.setEnabled(true);
        btnCrearCuenta2.setEnabled(true);
        btnOlvidarPass.setEnabled(true);
    }

    @Override
    public void showOlvidoActivity() {
        Intent intent = new Intent(acceder_activity.this, olvidarPass_activity.class);
        startActivity(intent);

    }

    @Override
    public void showCrearActivity() {
        Intent intent = new Intent(acceder_activity.this, crearCuenta_activity.class);
        startActivity(intent);
    }
}
