package com.misiontic.oldanime.view.explorar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

import com.misiontic.oldanime.Dororo;
import com.misiontic.oldanime.OnePiece;
import com.misiontic.oldanime.R;
import com.misiontic.oldanime.bleach;
import com.misiontic.oldanime.hunterxhunter;
import com.misiontic.oldanime.jojos;
import com.misiontic.oldanime.naruto;
import com.misiontic.oldanime.pokemon;
import com.misiontic.oldanime.saint;

public class aventura_activity extends AppCompatActivity {

    ImageButton ib1,ib2,ib3,ib4,ib5,ib6,ib7,ib8,ib9,ib10,ib11,ib12,ib13,ib14,ib15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aventura);

        ib1 = findViewById(R.id.ib_one_piece);
        ib1.setOnClickListener(v -> onePiece());

        ib2 = findViewById(R.id.ib_dororo);
        ib2.setOnClickListener(v -> dororo());

        ib3 = findViewById(R.id.ib_jojos);
        ib3.setOnClickListener(v -> jojos());

        ib4 = findViewById(R.id.ib_hxh);
        ib4.setOnClickListener(v -> hxh());

        ib5 = findViewById(R.id.ib_bleach);
        ib5.setOnClickListener(v -> bleach());

        ib6 = findViewById(R.id.ib_naruto);
        ib6.setOnClickListener(v -> onePiece());

        ib7 = findViewById(R.id.ib_pokemon);
        ib7.setOnClickListener(v -> pokemon());

        ib8 = findViewById(R.id.ib_saint);
        ib8.setOnClickListener(v -> saint());

        //ib9 = findViewById(R.id.ib_digimon);
        //ib9.setOnClickListener(v -> digimon());

        //ib10 = findViewById(R.id.ib_fma);
        //ib10.setOnClickListener(v -> full());

        //ib11 = findViewById(R.id.ib_inuyasha);
        //ib11.setOnClickListener(v -> inuyasha());

        //ib12 = findViewById(R.id.ib_black);
        //ib12.setOnClickListener(v -> black());

        //ib13 = findViewById(R.id.ib_shingeky);
        //ib13.setOnClickListener(v -> shingeky());

        //ib14 = findViewById(R.id.ib_ippo);
        //ib14.setOnClickListener(v -> ippo());

        //ib15 = findViewById(R.id.ib_tensei);
        //ib15.setOnClickListener(v -> tensei());

    }

    private void onePiece() {
        startActivity(new Intent(aventura_activity.this, OnePiece.class));
    }
    private void dororo() { startActivity(new Intent(aventura_activity.this, Dororo.class)); }
    private void jojos() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    private void hxh() { startActivity(new Intent(aventura_activity.this, hunterxhunter.class)); }
    private void bleach() { startActivity(new Intent(aventura_activity.this, bleach.class)); }
    private void naruto() { startActivity(new Intent(aventura_activity.this, naruto.class)); }
    private void pokemon() { startActivity(new Intent(aventura_activity.this, pokemon.class)); }
    private void saint() { startActivity(new Intent(aventura_activity.this, saint.class)); }
    //private void digimon() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void full() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void inuyasha() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void black() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void shingeky() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void ippo() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
    //private void tensei() { startActivity(new Intent(aventura_activity.this, jojos.class)); }
}