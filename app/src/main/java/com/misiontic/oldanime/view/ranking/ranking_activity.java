package com.misiontic.oldanime.view.ranking;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misiontic.oldanime.R;
import com.misiontic.oldanime.view.explorar.category_activity;
import com.misiontic.oldanime.view.inicio.inicio_activity;
import com.misiontic.oldanime.view.listas.lista_activity;

import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.models.BarModel;

public class ranking_activity extends AppCompatActivity {

    private AutoCompleteTextView spinner1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        //Desplegable ranking países
        spinner1 = (AutoCompleteTextView) findViewById(R.id.rankingPais);

        String [] listaPaises = {"pais 1","Pais 2","pais 3","Pais 4"};

        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listaPaises);
        spinner1.setAdapter(adapter);

        //BarGraph
        BarChart mBarChart = (BarChart) findViewById(R.id.barchart);

        mBarChart.addBar(new BarModel("Anime 1",2.3f, 0xFF123456));
        mBarChart.addBar(new BarModel("Anime 2",2.f,  0xFF343456));
        mBarChart.addBar(new BarModel("Anime 3",3.3f, 0xFF563456));
        mBarChart.addBar(new BarModel("Anime 4",1.1f, 0xFF873F56));
        //mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
        //mBarChart.addBar(new BarModel(2.f,  0xFF343456));
        //mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
        //mBarChart.addBar(new BarModel(4.f,  0xFF1BA4E6));

        mBarChart.startAnimation();




        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setSelectedItemId((R.id.page_4));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.page_1:
                        startActivity(new Intent(getApplicationContext(), inicio_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;

                    case R.id.page_2:
                        startActivity(new Intent(getApplicationContext(), lista_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;


                    case R.id.page_3:
                        startActivity(new Intent(getApplicationContext(), category_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;


                    case R.id.page_4:
                        startActivity(new Intent(getApplicationContext(),ranking_activity.class));
                        finish();
                        overridePendingTransition(0,0);
                        return false;



                }
                return false;
            }
        });



            }
}