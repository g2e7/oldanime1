package com.misiontic.oldanime.view.explorar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

import com.misiontic.oldanime.R;

public class accion_activity extends AppCompatActivity {

    ImageButton ib1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accion);

        ib1 = findViewById(R.id.ib_trigun);
        ib1.setOnClickListener(v -> sinopsis_anime());
    }

    private void sinopsis_anime() {
        startActivity(new Intent(accion_activity.this, trigun_Activity.class));
    }
}