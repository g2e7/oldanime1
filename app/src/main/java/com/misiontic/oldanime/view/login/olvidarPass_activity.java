package com.misiontic.oldanime.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.*;
import com.misiontic.oldanime.R;

public class olvidarPass_activity extends AppCompatActivity {

    private LinearProgressIndicator pbWait;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    AppCompatButton btnRecuperar;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olvidar_pass);

        initUI();
    }

    @SuppressLint("WrongViewCast")
    private void initUI() {
        pbWait = findViewById(R.id.pb_wait);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        btnRecuperar = findViewById(R.id.btn_recuperar);
        btnRecuperar.setOnClickListener(v -> onOlvidarContraseñaClick());
    }

    private void onOlvidarContraseñaClick() {
        Intent intent = new Intent(olvidarPass_activity.this, newPass_activity.class);
        startActivity(intent);
    }

}