package com.misiontic.oldanime.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.misiontic.oldanime.model.database.dao.AnimesDao;
import com.misiontic.oldanime.model.database.dao.UserDao;
import com.misiontic.oldanime.model.database.entity.*;

@Database(entities = {User.class, Animes.class}, version = 1)
public abstract class AnimeDatabase extends RoomDatabase {

    private volatile static AnimeDatabase instance;

    public static AnimeDatabase getInstance(Context context){
        if (instance == null ){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AnimeDatabase.class,"anime-database")
            .allowMainThreadQueries()
            .build();
        }

        return instance;
    }

    public abstract UserDao getUserDao();
    public abstract AnimesDao getAnimesDao();
}
