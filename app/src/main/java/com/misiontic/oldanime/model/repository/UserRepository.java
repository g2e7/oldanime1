package com.misiontic.oldanime.model.repository;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.misiontic.oldanime.model.database.AnimeDatabase;
import com.misiontic.oldanime.model.database.dao.UserDao;
import com.misiontic.oldanime.model.database.entity.User;

public class UserRepository {

    private final UserDao userDao;
    private final DatabaseReference userRef;
    private final Boolean inDB = true;

    public UserRepository(Context context) {
        this.userDao = AnimeDatabase.getInstance(context)
                .getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("message");

        loadInitialUsers();

    }

    private void loadInitialUsers() {

        if (inDB) {
            // Usar Room Database
            userDao.insert(new User("old", "old@gmail.com", "12345678"));
            userDao.insert(new User("guest", "guest@gmail.com", "12345678"));
            userDao.insert(new User("user3", "user3@gmail.com", "iamuser3"));

        } else {
            // Usar Firebase
            userRef.child("old_gmail_com").child("name").setValue("old");
            userRef.child("old_gmail_com").child("email").setValue("old@gmail.com");
            userRef.child("old_gmail_com").child("password").setValue("12345678");


            User user = new User("test", "test@gmail.com", "11223344");

            userRef.child(getEmailId(user.getEmail())).setValue(user);
        }
    }

    public void getUserByEmail(String email, getUserByEmailCallback callback) {

        if (inDB) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            userRef.child(getEmailId(email))
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            userRef.removeEventListener(this);
                            callback.onSuccess(user);
                        }
                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            userRef.removeEventListener(this);
                            callback.onFailure();
                        }
                    });
        }
    }

    private String getEmailId(String email) {
        return email.replace('@', '_').replace('.', '_');
    }

    public interface getUserByEmailCallback {
        void onSuccess(User user);

        void onFailure();
    }
}
