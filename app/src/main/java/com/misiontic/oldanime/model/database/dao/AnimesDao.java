package com.misiontic.oldanime.model.database.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.misiontic.oldanime.model.database.entity.Animes;

import java.util.List;

@Dao
public interface AnimesDao {

    @Query("SELECT * FROM animes")
    List<Animes> getAll();
}
