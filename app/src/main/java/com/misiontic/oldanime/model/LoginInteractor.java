package com.misiontic.oldanime.model;

import android.content.Context;

import com.misiontic.oldanime.model.database.entity.User;
import com.misiontic.oldanime.model.repository.UserRepository;
import com.misiontic.oldanime.mvp.LoginMVP;

import java.util.Map;

public class LoginInteractor implements LoginMVP.Model {


    // private Map<String, String> users;
    private final UserRepository userRepository;

    public LoginInteractor(Context context) {
        userRepository = new UserRepository(context); }

    @Override
    public void validateCredentials(String email, String password, validateCredentialsCallbacks callback) {
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        userRepository.getUserByEmail(email, new UserRepository.getUserByEmailCallback() {
            @Override
            public void onSuccess(User user) {
                if (user != null && user.getPassword().equals(password)) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });


    }
}
